import boto3
import pandas as pd
import io
from pathlib import Path
import os
import re
import shutil

aws_access_key_id="your access key"
aws_secret_access_key="your secret key"

BUCKET_NAME = 'prod.blt.upload.uaa.aurisinternal.net'

session = boto3.Session(
    aws_access_key_id=aws_access_key_id,
    aws_secret_access_key=aws_secret_access_key
)
s3r = session.resource('s3')
bucket = s3r.Bucket(BUCKET_NAME)

s3c = boto3.client(
        's3',
        aws_access_key_id = aws_access_key_id,
        aws_secret_access_key = aws_secret_access_key
    )


dl = "/home/dzhan111@na.jnj.com/DatabaseS3/studyData_S3_script_download/"
os.chdir(dl)


for my_bucket_object in bucket.objects.filter(Prefix = 'studyData/2023'):
    if re.search('(studyData\/[0-9]{4}-[0-9]{2}-[0-9]{2}\/SYSTEM1[A-Za-z0-9_]*\/[A-Za-z0-9{}-]*\/case_data)', my_bucket_object.key):
#        p_ds = re.match('studyData\/([0-9]{4}-[0-9]{2}-[0-9]{2})\/', my_bucket_object.key).group(1)
#        p_sys = re.match('studyData\/[0-9]{4}-[0-9]{2}-[0-9]{2}\/([A-Za-z0-9_]*)\/', my_bucket_object.key).group(1)
#        s3_bundle = re.match('studyData\/[0-9]{4}-[0-9]{2}-[0-9]{2}\/[A-Za-z0-9_]*\/([A-Za-z0-9{}-]*)\/', my_bucket_object.key).group(1)
        filepath = my_bucket_object.key
        
        path, filename = os.path.split(filepath)
        
        if filename.endswith((".txt", ".vtk", ".mhd", ".raw")) == False:
            continue
        
        s3c.download_file(BUCKET_NAME, my_bucket_object.key, filename)
        print(filename)
        
        if filename == "case.pb.txt":

            with open("case.pb.txt", "r") as file_input:
                #rename the folder using uuid
                uuid = [next(file_input) for x in range(1)]
                uuid = re.split('"',uuid[0])[1]
#                    print(uuid)
            file_input.close()
            #remove userinfo
            tag_found = False    
            with open("case.pb.txt", "r") as file_input:
                with open("temp.txt", "w") as file_output:
                    lines = file_input.readlines()
                    for line in lines:
                        # check if string present on a current line
                        if line.find("unplanned") != -1:
#                            print('Line Number:', lines.index(line))
#                            print('Line:', line)
                            tag_found = False
                        
                        if line.find("userinfo") != -1:
#                            print('Line Number:', lines.index(line))
#                            print('Line:', line)
                            tag_found = True
                        
                        if tag_found == False:
                            file_output.write(line)            
                file_input.close()
                file_output.close()
                os.replace('temp.txt', 'case.pb.txt')

            folderpath = dl + uuid
            if os.path.exists(folderpath) == True:
                shutil.rmtree(folderpath)
                os.makedirs(folderpath)
            else:
                os.makedirs(folderpath)  
            
            shutil.move(dl+filename, dl+uuid+'/'+filename)
        else:
            shutil.move(dl+filename, dl+uuid+'/'+filename)
            

#import boto3
#import pandas as pd
#import io
#from pathlib import Path
#import os
#import re
#​
#aws_access_key_id="your access key"
#aws_secret_access_key="your secret ke"
#​
#BUCKET_NAME = 'prod.blt.upload.uaa.aurisinternal.net'
#​
#session = boto3.Session(
#    aws_access_key_id=aws_access_key_id,
#    aws_secret_access_key=aws_secret_access_key
#)
#s3r = session.resource('s3')
#bucket = s3r.Bucket(BUCKET_NAME)
#​
#s3c = boto3.client(
#        's3',
#        aws_access_key_id = aws_access_key_id,
#        aws_secret_access_key = aws_secret_access_key
#    )
#​
#​
#dl = Path(r'C:\Users\gnewick\Downloads\segment_files')
#os.chdir(dl)
#​
#​
#for my_bucket_object in bucket.objects.filter(Prefix = 'studyData/2022'):
#    if re.search('(studyData\/[0-9]{4}-[0-9]{2}-[0-9]{2}\/SYSTEM1[A-Za-z0-9_]*\/[A-Za-z0-9{}-]*\/case_data)', my_bucket_object.key):
#        p_ds = re.match('studyData\/([0-9]{4}-[0-9]{2}-[0-9]{2})\/', my_bucket_object.key).group(1)
#        p_sys = re.match('studyData\/[0-9]{4}-[0-9]{2}-[0-9]{2}\/([A-Za-z0-9_]*)\/', my_bucket_object.key).group(1)
#        s3_bundle = re.match('studyData\/[0-9]{4}-[0-9]{2}-[0-9]{2}\/[A-Za-z0-9_]*\/([A-Za-z0-9{}-]*)\/', my_bucket_object.key).group(1)
#        filepath = my_bucket_object.key
#​
#        for file in ['case\.pb\.txt', 'segment\.csv', 'segment_cp\.csv']:
#            if re.search(f'({file})', my_bucket_object.key):
#                filename = p_ds + '_' + p_sys + '_' + s3_bundle + '_' + file.replace('\.', '.')
#​
#                print(filename)
#                s3c.download_file(BUCKET_NAME, my_bucket_object.key, filename)
#                break
