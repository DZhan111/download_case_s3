This repo includes a Python script to download cases from S3 database. 

Please note only `".txt", ".vtk", ".mhd", ".raw"` files will be downloaded. 

Meanwhile, the downloaded case `folder name` and `case.pb.txt` are modified to be correctly read in Pre-Op planning software. The folder name is changed to the `uuid` found in `case.pb.txt` and the `userinfo` lines are removed from `case.pb.txt`.

